(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _page_club_page_club_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page-club/page-club.component */ "./src/app/page-club/page-club.component.ts");
/* harmony import */ var _page_home_page_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-home/page-home.component */ "./src/app/page-home/page-home.component.ts");
/* harmony import */ var _page_where_to_play_page_where_to_play_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page-where-to-play/page-where-to-play.component */ "./src/app/page-where-to-play/page-where-to-play.component.ts");
/* harmony import */ var _page_latest_news_page_latest_news_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-latest-news/page-latest-news.component */ "./src/app/page-latest-news/page-latest-news.component.ts");
/* harmony import */ var _blog_la_liga_official_outfitter_la_liga_official_outfitter_page_la_liga_official_outfitter_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.ts");



// Main Pages




// Blog Pages

var routes = [
    { path: '', component: _page_home_page_home_component__WEBPACK_IMPORTED_MODULE_4__["PageHomeComponent"] },
    { path: 'club', component: _page_club_page_club_component__WEBPACK_IMPORTED_MODULE_3__["PageClubComponent"] },
    { path: 'play', component: _page_where_to_play_page_where_to_play_component__WEBPACK_IMPORTED_MODULE_5__["PageWhereToPlayComponent"] },
    { path: 'news', component: _page_latest_news_page_latest_news_component__WEBPACK_IMPORTED_MODULE_6__["PageLatestNewsComponent"] },
    { path: 'news/official-outfitter-la-liga', component: _blog_la_liga_official_outfitter_la_liga_official_outfitter_page_la_liga_official_outfitter_page_component__WEBPACK_IMPORTED_MODULE_7__["LaLigaOfficialOutfitterPageComponent"] }
];
// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { enableTracing: true } // <-- debugging purposes only
                )
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<!-- <div style=\"text-align:center\">\n  <h1>\n    Welcome to {{ title }}!\n  </h1>\n  <img width=\"300\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\n</div>\n<h2>Here are some links to help you start: </h2>\n<ul>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/cli\">CLI Documentation</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://blog.angular.io/\">Angular blog</a></h2>\n  </li>\n</ul>\n\n<router-outlet></router-outlet> -->\n\n<app-main-menu></app-main-menu>\n<router-outlet></router-outlet>\n<app-footer></app-footer> "

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        this.title = 'ftfp-web';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _main_menu_main_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main-menu/main-menu.component */ "./src/app/main-menu/main-menu.component.ts");
/* harmony import */ var _page_club_page_club_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-club/page-club.component */ "./src/app/page-club/page-club.component.ts");
/* harmony import */ var _page_where_to_play_page_where_to_play_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-where-to-play/page-where-to-play.component */ "./src/app/page-where-to-play/page-where-to-play.component.ts");
/* harmony import */ var _page_home_page_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-home/page-home.component */ "./src/app/page-home/page-home.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _page_latest_news_page_latest_news_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./page-latest-news/page-latest-news.component */ "./src/app/page-latest-news/page-latest-news.component.ts");
/* harmony import */ var _blog_la_liga_official_outfitter_la_liga_official_outfitter_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./blog/la-liga-official-outfitter/la-liga-official-outfitter.component */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.ts");
/* harmony import */ var _blog_la_liga_official_outfitter_la_liga_official_outfitter_page_la_liga_official_outfitter_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.ts");













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _main_menu_main_menu_component__WEBPACK_IMPORTED_MODULE_5__["MainMenuComponent"],
                _page_club_page_club_component__WEBPACK_IMPORTED_MODULE_6__["PageClubComponent"],
                _page_where_to_play_page_where_to_play_component__WEBPACK_IMPORTED_MODULE_7__["PageWhereToPlayComponent"],
                _page_home_page_home_component__WEBPACK_IMPORTED_MODULE_8__["PageHomeComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"],
                _page_latest_news_page_latest_news_component__WEBPACK_IMPORTED_MODULE_10__["PageLatestNewsComponent"],
                _blog_la_liga_official_outfitter_la_liga_official_outfitter_component__WEBPACK_IMPORTED_MODULE_11__["LaLigaOfficialOutfitterComponent"],
                _blog_la_liga_official_outfitter_la_liga_official_outfitter_page_la_liga_official_outfitter_page_component__WEBPACK_IMPORTED_MODULE_12__["LaLigaOfficialOutfitterPageComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-blog d-table w-100\">\n  <div class=\"d-table-cell align-middle h-100\">\n    <img class=\"header-bg\" src=\"assets/images/photo-2.jpg\"/>\n    <div class=\"gradient-bg\"></div>\n    <h1>Official Outfitter of the Touch Association Pilipinas</h1>\n  </div>\n</header>\n\n<section class=\"container news-container\">\n  <p>La Liga is a school of thought that was drawn up by our national hero; José Rizal.</p>\n  <p>Where through his travels, he returns to the Philippines and then shares his experiences and knowledge onto his countrymen.</p>\n  <p>Now our founder/ceo, Paulo Jordan, with similar intent, has organically evolved the very same concept from not just a mentality of helping others but now into a trademarked active lifestyle brand, LA LIGA™, in addition to being a premium bespoke teamwear manufacturer.</p>\n  <p>Not to be confused by the Spanish La Liga, LA LIGA was founded by a former National Athlete who, with the help of friends, established a brand where he will infuse all interests and knowledge into one consistent brand; one LA LIGA familia brand.</p>\n  <p>Why LA LIGA? One may ask. LA LIGA was derived from Philippine's National Hero, José Protacio Rizal Mercado y Alonso Realonda. In 1892 our beloved José Rizal established LA LIGA Filipina where he scouted and recruited a group that sought to involve the people directly toward his reform movement. Through LA LIGA Filipina he provided a mutual aid and a self-help society dispersing scholarship funds, legal aid, loaning capital and provided support where his peers were successful in creating cooperatives.</p>\n  <p>(LA LIGA) Academia is the founder's personal initiative toward providing a vehicle for impoverished children of the Philippines through the sport of rugby where he will voluntarily share and widen the children's perspective in how to be and carry themselves as a proper atleta, (athlete in tagalog).</p>\n  <p>14% of any monetary gains we receive at LA LIGA will always and forever be donated toward sport advocacies, where the betterment and longevity is reciprocated back to the children of the Philippines, beginning with the sport of Rugby.</p>\n  <p>La Liga is the official outfitter of the Philippine Pythons - the Philippine National Touch Football Team.</p>\n  <p>Get a 10% discount for all touch football team orders by quoting the promo code “TAP10” on your next order. Contact Paulo Jordan at 0917.672.1414 or ask@laliga.com to order your bespoke team kits.</p>\n</section>"

/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.scss":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.scss ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Jsb2cvbGEtbGlnYS1vZmZpY2lhbC1vdXRmaXR0ZXIvbGEtbGlnYS1vZmZpY2lhbC1vdXRmaXR0ZXItcGFnZS9sYS1saWdhLW9mZmljaWFsLW91dGZpdHRlci1wYWdlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: LaLigaOfficialOutfitterPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaLigaOfficialOutfitterPageComponent", function() { return LaLigaOfficialOutfitterPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LaLigaOfficialOutfitterPageComponent = /** @class */ (function () {
    function LaLigaOfficialOutfitterPageComponent() {
    }
    LaLigaOfficialOutfitterPageComponent.prototype.ngOnInit = function () {
    };
    LaLigaOfficialOutfitterPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-la-liga-official-outfitter-page',
            template: __webpack_require__(/*! ./la-liga-official-outfitter-page.component.html */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.html"),
            styles: [__webpack_require__(/*! ./la-liga-official-outfitter-page.component.scss */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LaLigaOfficialOutfitterPageComponent);
    return LaLigaOfficialOutfitterPageComponent;
}());



/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container article-section\">\n  <h1>\n    <a href=\"news/official-outfitter-la-liga\">Official Outfitter of the Touch Association Pilipinas</a>\n  </h1>\n  <h6>\n    <label class=\"category\">News</label>\n    <label class=\"date\">February 25, 2019</label>\n  </h6>\n  <figure>\n    <img class=\"w-100\" src=\"assets/images/photo-1.jpg\"/>\n    <div class=\"article-bg\"></div>\n  </figure>\n  <div class=\"artile-headline\">\n    <p>Why LA LIGA? One may ask. LA LIGA was derived from Philippine's National Hero, José Protacio Rizal Mercado y Alonso Realonda. In 1892 our beloved José Rizal established LA LIGA Filipina where he scouted and recruited a group that sought to involve the people directly toward his reform movement. Through LA LIGA Filipina he provided a mutual aid and a self-help society dispersing scholarship funds, legal aid, loaning capital and provided support where his peers were successful in creating cooperatives.</p>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Jsb2cvbGEtbGlnYS1vZmZpY2lhbC1vdXRmaXR0ZXIvbGEtbGlnYS1vZmZpY2lhbC1vdXRmaXR0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: LaLigaOfficialOutfitterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaLigaOfficialOutfitterComponent", function() { return LaLigaOfficialOutfitterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LaLigaOfficialOutfitterComponent = /** @class */ (function () {
    function LaLigaOfficialOutfitterComponent() {
    }
    LaLigaOfficialOutfitterComponent.prototype.ngOnInit = function () {
    };
    LaLigaOfficialOutfitterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-la-liga-official-outfitter',
            template: __webpack_require__(/*! ./la-liga-official-outfitter.component.html */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.html"),
            styles: [__webpack_require__(/*! ./la-liga-official-outfitter.component.scss */ "./src/app/blog/la-liga-official-outfitter/la-liga-official-outfitter.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LaLigaOfficialOutfitterComponent);
    return LaLigaOfficialOutfitterComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer>\n  <div class=\"container logo-tap-shield\">\n    <img class=\"\" src=\"assets/images/TAP-Shield.png\"/>\n  </div> \n  <div class=\"desktop-container\">\n    <section class=\"sponsors container\">\n        <h1>Our Sponsors</h1>\n    </section>\n    <section class=\"sitemap container\">\n        <h1>Sitemap</h1>\n        <ul>\n          <li><a href=\"#\">Home</a></li>\n          <li><a href=\"#\">Meet the Clubs</a></li>\n          <li><a href=\"#\">Where to Play?</a></li>\n          <li><a href=\"#\">Latest News</a></li>\n        </ul>\n    </section>\n    <section class=\"sitemap container\">\n        <h1>Socials</h1>\n        <ul>\n          <li><a href=\"https://wwww.facebook.com/Touch.Football.Pilipinas\">Facebook</a></li>\n          <li><a href=\"https://instagram.com/touchfootballph\">Instragram</a></li>\n          <li><a href=\"#\">Twitter</a></li>\n        </ul>\n    </section>\n  </div>\n  <div class=\"footer-btm\">Touch Association Pilipinas</div>\n</footer>"

/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/main-menu/main-menu.component.html":
/*!****************************************************!*\
  !*** ./src/app/main-menu/main-menu.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n  <img class=\"menu-toggle\" src=\"/assets/images/MainNavigationIcon.png\"/>\n  <!-- desktop menu items -->\n  <ul class=\"desktop-menu-items\">\n    <li><img src=\"assets/images/TAP-Abbr.png\"/></li>\n    <li><a routerLink=\"/\" href=\"/\">Home</a></li>\n    <li><a routerLink=\"/club\" href=\"/club\">Meet the Clubs</a></li>\n    <li><a routerLink=\"/play\" href=\"/play\">Where to Play?</a></li>\n    <li><a routerLink=\"/news\" href=\"/news\">News</a></li>\n  </ul>\n  \n  <!-- popup menu -->\n  <section class=\"menu-contents h-100 w-100\">\n    <div class=\"d-table-cell align-middle\">\n      <ul>\n        <li><a routerLink=\"/\" href=\"/\">Home</a></li>\n        <li><a routerLink=\"/club\" href=\"/club\">Meet the Clubs</a></li>\n        <li><a routerLink=\"/play\" href=\"/play\">Where to Play?</a></li>\n        <li><a routerLink=\"/news\" href=\"/news\">News</a></li>\n      </ul>\n    </div>\n    <img class=\"vector-2\" src=\"assets/images/vector-2.png\"/>\n  </section>\n</nav>"

/***/ }),

/***/ "./src/app/main-menu/main-menu.component.scss":
/*!****************************************************!*\
  !*** ./src/app/main-menu/main-menu.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4tbWVudS9tYWluLW1lbnUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/main-menu/main-menu.component.ts":
/*!**************************************************!*\
  !*** ./src/app/main-menu/main-menu.component.ts ***!
  \**************************************************/
/*! exports provided: MainMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainMenuComponent", function() { return MainMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);



var MainMenuComponent = /** @class */ (function () {
    function MainMenuComponent() {
    }
    MainMenuComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__(document).ready(function () {
            // Menu Toggle
            jquery__WEBPACK_IMPORTED_MODULE_2__('.menu-toggle').click(function () {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.menu-contents').addClass('show');
            });
            jquery__WEBPACK_IMPORTED_MODULE_2__('.menu-contents a').click(function () {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.menu-contents').removeClass('show');
            });
            // Window Scroll
            jquery__WEBPACK_IMPORTED_MODULE_2__(window).scroll(function () {
                if (jquery__WEBPACK_IMPORTED_MODULE_2__(window).scrollTop() > 0) {
                    jquery__WEBPACK_IMPORTED_MODULE_2__('app-main-menu').css('background', 'rgba(0, 0, 0, 0.80)');
                }
                else {
                    jquery__WEBPACK_IMPORTED_MODULE_2__('app-main-menu').css('background', 'none');
                }
            });
        });
    };
    MainMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-menu',
            template: __webpack_require__(/*! ./main-menu.component.html */ "./src/app/main-menu/main-menu.component.html"),
            styles: [__webpack_require__(/*! ./main-menu.component.scss */ "./src/app/main-menu/main-menu.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainMenuComponent);
    return MainMenuComponent;
}());



/***/ }),

/***/ "./src/app/page-club/page-club.component.html":
/*!****************************************************!*\
  !*** ./src/app/page-club/page-club.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-child d-table w-100\">\n  <div class=\"d-table-cell align-middle h-100\">\n    <img class=\"header-bg\" src=\"assets/images/photo-2.jpg\"/>\n    <div class=\"gradient-bg\"></div>\n    <h1>Meet the Clubs</h1>\n  </div>\n</header>\n<section class=\"container section-container\">\n  <header class=\"header-title\">\n    <img class=\"header-title-bg\" src=\"assets/images/img-header-latestnews.png\"/>\n    <h1><img src=\"assets/images/img-header-icon.png\"/>Meet the Clubs</h1>\n  </header>\n  <div class=\"club-list\">\n    <div class=\"club-logo\">\n        <img class=\"club-image-1\" src=\"assets/images/team-uptf.png\"/>\n        <section class=\"description\">\n            <div class=\"background-bg\"></div>\n            <div class=\"paragraph\">\n                <img class=\"w-50 mb-3\" src=\"assets/images/team-uptf.png\"/>\n                <h1>UP Touch Football Club</h1>\n                <p>Founded in February 2006, the UP Touch Football Club (UPTFC) made its mark as the premier organization in the University of the Philippines Diliman to introduce touch football as a sport to the student populace.</p>\n                <p>The club started as a sister team to the Nomads Bullettes and  eventually became an independent organization. With six founding members, the club has grown today with a membership base of 40+ active members and is continually expanding.</p>\n            </div>\n        </section>\n    </div>\n    <div class=\"club-logo\">\n        <img class=\"club-image-1\" src=\"assets/images/team-nomads.png\"/>\n        <section class=\"description\">\n            <div class=\"background-bg\"></div>\n            <div class=\"paragraph\">\n                <img class=\"w-50 mb-3\" src=\"assets/images/team-nomads.png\"/>\n                <h1>Manila Nomads</h1>\n                <p>Founded in 2004, the Nomads Bullettes is the pioneering ladies club team that introduced club touch rugby in the Philippines. The Nomads Bullettes participate in numerous local and international competitions and have carried the Philippine flag in 4 Asian Touch Championships.</p>\n                <p>The Bullettes are the ladies touch team of the Manila Nomads - Nomads began in 1914, the second oldest sporting club in the Philippines, originally serving as a recreational outlet for British expatriates.</p>\n                <p>Today, Nomads Rugby Club proudly boasts a number of representatives in the Philippine Volcanoes national team, the Philippine Residents XV, and the National Development Team (for Philippine Heritage players).</p>\n                <p>Nomads Rugby seeks to espouse rugby’s core values of Integrity, Passion, Solidarity, Discipline, and Respect. The club strives to promote rugby as proud ambassadors of these values, and developing grass-roots rugby as well as representing our nation at the highest level.</p>\n            </div>\n        </section>\n    </div>\n    <div class=\"club-logo\">\n        <img class=\"club-image-1\" src=\"assets/images/team-laliga.jpg\"/>\n        <section class=\"description\">\n            <div class=\"background-bg\"></div>\n            <div class=\"paragraph\">\n                <img class=\"w-50 mb-3\" src=\"assets/images/team-laliga.jpg\"/>\n                <h1>La Liga</h1>\n                <p>La Liga Academia is a 7s Rugby academy focused on character & athlete development.</p>\n                <p>Built from the ground up off a sincere premise with one mission - to inspire the next. The premise of instilling change in matters we can no longer sit for nor stay silent for. The co-founders, former Philippine Volcano teammates, are simply executing their own interpretation of the sport of rugby through various initiatives as seen through @laligaacademia's facebook footprint.</p>\n                <p>Follow our #inspirethenext movement/cause as we continue to redefine and pursue our vision of professionalizing the first rugby academy in the Philippines.</p>\n                <p>\"Service to others is the rent you pay for your room here on earth.\" - Cassius Clay</p>\n            </div>\n        </section>\n    </div>\n    <div class=\"club-logo\">\n        <img class=\"club-image-1\" src=\"assets/images/team-mtfc.png\"/>\n        <section class=\"description\">\n            <div class=\"background-bg\"></div>\n            <div class=\"paragraph\">\n                <img class=\"w-50 mb-3\" src=\"assets/images/team-mtfc.png\"/>\n                <h1>Makati Touch Football Club</h1>\n                <p>Makati Touch Football Club was founded in 2016 by a group of family and friends who love the sport and are deeply committed to growing within the touch rugby community. MTFC plans to further the sport of touch in the Philippines by fielding top teams to support local and international tournaments, and by providing great facilities and practices for people to participate in. MTFC believes in training hard, having fun, and never forgetting the love of the game</p>\n            </div>\n        </section>\n    </div>\n  </div>\n</section>\n\n<!-- Affifiate Clubs -->\n\n<section class=\"container section-container\">\n  <header class=\"header-title\">\n    <img class=\"header-title-bg\" src=\"assets/images/img-header-latestnews.png\"/>\n    <h1><img src=\"assets/images/img-header-icon.png\"/>Affifiate Clubs</h1>\n  </header>\n  <div class=\"club-list\">\n    <div class=\"club-logo\">\n        <img class=\"club-image-1\" src=\"assets/images/team-mtr.png\"/>\n        <section class=\"description\">\n            <div class=\"background-bg\"></div>\n            <div class=\"paragraph\">\n                <img class=\"w-50 mb-3\" src=\"assets/images/team-mtr.png\"/>\n                <h1>UP Touch Football Club</h1>\n                <p>Founded in February 2006, the UP Touch Football Club (UPTFC) made its mark as the premier organization in the University of the Philippines Diliman to introduce touch football as a sport to the student populace.</p>\n                <p>The club started as a sister team to the Nomads Bullettes and  eventually became an independent organization. With six founding members, the club has grown today with a membership base of 40+ active members and is continually expanding.</p>\n            </div>\n        </section>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/page-club/page-club.component.scss":
/*!****************************************************!*\
  !*** ./src/app/page-club/page-club.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtY2x1Yi9wYWdlLWNsdWIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/page-club/page-club.component.ts":
/*!**************************************************!*\
  !*** ./src/app/page-club/page-club.component.ts ***!
  \**************************************************/
/*! exports provided: PageClubComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageClubComponent", function() { return PageClubComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);



var PageClubComponent = /** @class */ (function () {
    function PageClubComponent() {
    }
    PageClubComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__('.club-image-1').on('click', function () {
            jquery__WEBPACK_IMPORTED_MODULE_2__(this).next().addClass('show');
        });
        jquery__WEBPACK_IMPORTED_MODULE_2__('.background-bg').on('click', function () {
            jquery__WEBPACK_IMPORTED_MODULE_2__(this).parent().removeClass('show');
        });
    };
    PageClubComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-club',
            template: __webpack_require__(/*! ./page-club.component.html */ "./src/app/page-club/page-club.component.html"),
            styles: [__webpack_require__(/*! ./page-club.component.scss */ "./src/app/page-club/page-club.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageClubComponent);
    return PageClubComponent;
}());



/***/ }),

/***/ "./src/app/page-home/page-home.component.html":
/*!****************************************************!*\
  !*** ./src/app/page-home/page-home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-home\">\n  <img class=\"header-home-bg\" src=\"assets/images/photo-2.jpg\"/>\n  <div class=\"gradient-bg\"></div>\n  <figure class=\"h-100 text-center align-middle\">\n    <img class=\"tap-logo\" src=\"assets/images/TAP-Full.png\"/>\n  </figure>\n</header>\n\n<section class=\"container section-container\">\n  <header class=\"header-title\">\n    <img class=\"header-title-bg\" src=\"assets/images/img-header-latestnews.png\"/>\n    <h1><img src=\"assets/images/img-header-icon.png\"/>About Us</h1>\n  </header>\n  <div class=\"home-about-photo mt-4 mb-4\">\n    <img class=\"w-100\" src=\"assets/images/photo-1.jpg\"/>\n  </div>\n  <h1 class=\"header-title-2\">Get in Touch</h1>\n  <p>Formerly known as the Federation of Touch Football Pilipinas, Touch Association Pilipinas (or TAP for short), is the national sports association for the sport of touch rugby/football in the Philippines.</p>\n  <p>TAP is a member nation of the Federation of International Touch(FIT) - the international governing body of the sport.</p>\n  <a href=\"#\" class=\"btn-default\">Learn more</a>\n</section>\n\n<section class=\"break-photo\">\n  <img class=\"w-100\" src=\"assets/images/soc1.png\"/>\n</section>\n\n<section class=\"container section-container position-relative\">\n  <header class=\"header-title\">\n    <img class=\"header-title-bg\" src=\"assets/images/img-header-latestnews.png\"/>\n    <h1><img src=\"assets/images/img-header-icon.png\"/>Play with us</h1>\n  </header>\n  <img class=\"image-home-1\" src=\"assets/images/img-red.png\"/>\n  <div class=\"home-about-photo mt-4 mb-4\">\n    <img class=\"w-100\" src=\"assets/images/photo-1.jpg\"/>\n  </div>\n  <p>Formerly known as the Federation of Touch Football Pilipinas, Touch Association Pilipinas (or TAP for short), is the national sports association for the sport of touch rugby/football in the Philippines.</p>\n  <a href=\"#\"  class=\"btn-default\">Learn more</a>\n</section>"

/***/ }),

/***/ "./src/app/page-home/page-home.component.scss":
/*!****************************************************!*\
  !*** ./src/app/page-home/page-home.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtaG9tZS9wYWdlLWhvbWUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/page-home/page-home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/page-home/page-home.component.ts ***!
  \**************************************************/
/*! exports provided: PageHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageHomeComponent", function() { return PageHomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageHomeComponent = /** @class */ (function () {
    function PageHomeComponent() {
    }
    PageHomeComponent.prototype.ngOnInit = function () {
    };
    PageHomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-home',
            template: __webpack_require__(/*! ./page-home.component.html */ "./src/app/page-home/page-home.component.html"),
            styles: [__webpack_require__(/*! ./page-home.component.scss */ "./src/app/page-home/page-home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageHomeComponent);
    return PageHomeComponent;
}());



/***/ }),

/***/ "./src/app/page-latest-news/page-latest-news.component.html":
/*!******************************************************************!*\
  !*** ./src/app/page-latest-news/page-latest-news.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-child d-table w-100\">\n  <div class=\"d-table-cell align-middle h-100\">\n    <img class=\"header-bg\" src=\"assets/images/photo-2.jpg\"/>\n    <div class=\"gradient-bg\"></div>\n    <h1>Latest News</h1>\n  </div>\n</header>\n\n<app-la-liga-official-outfitter></app-la-liga-official-outfitter>\n\n"

/***/ }),

/***/ "./src/app/page-latest-news/page-latest-news.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/page-latest-news/page-latest-news.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtbGF0ZXN0LW5ld3MvcGFnZS1sYXRlc3QtbmV3cy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/page-latest-news/page-latest-news.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/page-latest-news/page-latest-news.component.ts ***!
  \****************************************************************/
/*! exports provided: PageLatestNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageLatestNewsComponent", function() { return PageLatestNewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageLatestNewsComponent = /** @class */ (function () {
    function PageLatestNewsComponent() {
    }
    PageLatestNewsComponent.prototype.ngOnInit = function () {
    };
    PageLatestNewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-latest-news',
            template: __webpack_require__(/*! ./page-latest-news.component.html */ "./src/app/page-latest-news/page-latest-news.component.html"),
            styles: [__webpack_require__(/*! ./page-latest-news.component.scss */ "./src/app/page-latest-news/page-latest-news.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageLatestNewsComponent);
    return PageLatestNewsComponent;
}());



/***/ }),

/***/ "./src/app/page-where-to-play/page-where-to-play.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/page-where-to-play/page-where-to-play.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-child d-table w-100\">\n  <div class=\"d-table-cell align-middle h-100\">\n      <img class=\"header-bg\" src=\"assets/images/photo-2.jpg\"/>\n      <div class=\"gradient-bg\"></div>\n      <h1>Where to play?</h1>\n  </div>\n</header>\n<section class=\"container section-container\">\n  <img src=\"assets/images/photo-3.png\" class=\"w-100 mb-3\"/>\n  <h1 class=\"header-title-2\">Get in Touch</h1>\n  <p>Formerly known as the Federation of Touch Football Pilipinas, Touch Association Pilipinas (or TAP for short), is the national sports association for the sport of touch rugby/football in the Philippines.</p>\n</section>\n\n<section class=\"container play-content section-container\">\n  <div>\n    <div class=\"w-100\">\n      <figure>\n        <img class =\"w-100\" src=\"assets/images/team-mtr.png\"/>\n      </figure>\n    </div>\n    <div class=\"w-100\">\n        <h1 class=\"header-title-2\">Manila Touch Rugby</h1>\n        <p>Manila Touch Rugby (MTR) is a non-profit rugby organization created for people who want to play Touch Rugby in a fun, friendly, social setting. MTR welcomes adult players of all skill levels, experience, and genders.</p>\n        <ul>\n          <li>Every Wednesday at 7:30 PM - 9:00 PM</li>\n          <li>British School Manila</li>\n          <li>Pitch Fee: PHP 200/player</li>\n        </ul>\n        <p>Register every week at <a href=\"https://www.facebook.com/groups/926287517411761/\">MTR's Facebook Group</a></p>\n    </div>\n  </div>\n</section>\n\n<section class=\"container play-content section-container\">\n  <div>\n    <div class=\"w-100\">\n      <figure>\n        <img class =\"w-100\" src=\"assets/images/team-laliga.jpg\"/>\n      </figure>\n    </div>\n    <div class=\"w-100\">\n        <h1 class=\"header-title-2\">LARO by La Liga</h1>\n        <p>A NON-PROFIT initiative wherein the La Liga brand provides an open platform catering to the rugby & touch football community. Promoting character & athlete development. Open to entry & intermediate players #inspirethenext #laligaturo</p>\n        <ul>\n          <li>Every Sunday</li>\n          <li>British School Manila</li>\n          <li>Pitch Fee: PHP 200/player</li>\n        </ul>\n        <p>Register every week at <a href=\"https://www.facebook.com/groups/2212589772297253/\">La Liga's facebook page</a></p>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/page-where-to-play/page-where-to-play.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/page-where-to-play/page-where-to-play.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Utd2hlcmUtdG8tcGxheS9wYWdlLXdoZXJlLXRvLXBsYXkuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/page-where-to-play/page-where-to-play.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/page-where-to-play/page-where-to-play.component.ts ***!
  \********************************************************************/
/*! exports provided: PageWhereToPlayComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageWhereToPlayComponent", function() { return PageWhereToPlayComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageWhereToPlayComponent = /** @class */ (function () {
    function PageWhereToPlayComponent() {
    }
    PageWhereToPlayComponent.prototype.ngOnInit = function () {
    };
    PageWhereToPlayComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-where-to-play',
            template: __webpack_require__(/*! ./page-where-to-play.component.html */ "./src/app/page-where-to-play/page-where-to-play.component.html"),
            styles: [__webpack_require__(/*! ./page-where-to-play.component.scss */ "./src/app/page-where-to-play/page-where-to-play.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageWhereToPlayComponent);
    return PageWhereToPlayComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/princecarlesalazar/Documents/FTFP/ftfp-web-v2/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map