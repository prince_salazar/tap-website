import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { PageClubComponent } from './page-club/page-club.component';
import { PageWhereToPlayComponent } from './page-where-to-play/page-where-to-play.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { FooterComponent } from './footer/footer.component';
import { PageLatestNewsComponent } from './page-latest-news/page-latest-news.component';
import { LaLigaOfficialOutfitterComponent } from './blog/la-liga-official-outfitter/la-liga-official-outfitter.component';
import { LaLigaOfficialOutfitterPageComponent } from './blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    PageClubComponent,
    PageWhereToPlayComponent,
    PageHomeComponent,
    FooterComponent,
    PageLatestNewsComponent,
    LaLigaOfficialOutfitterComponent,
    LaLigaOfficialOutfitterPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
