import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $( document ).ready(function() {

      // Menu Toggle
      $('.menu-toggle').click(function(){
        $('.menu-contents').addClass('show');
      });
      $('.menu-contents a').click(function(){
        $('.menu-contents').removeClass('show');
      });

      // Window Scroll
      $(window).scroll(function() {
          if($(window).scrollTop() > 0) {
              $('app-main-menu').css('background','rgba(0, 0, 0, 0.80)');
          } else {
            $('app-main-menu').css('background','none');
          }
      });
    });
  }

}
