import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-page-club',
  templateUrl: './page-club.component.html',
  styleUrls: ['./page-club.component.scss']
})
export class PageClubComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.club-image-1').on('click', function(){
      $(this).next().addClass('show');
    });
    $('.background-bg').on('click', function(){
      $(this).parent().removeClass('show');
    });
  }

}
