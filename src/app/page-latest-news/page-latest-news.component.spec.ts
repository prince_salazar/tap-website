import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageLatestNewsComponent } from './page-latest-news.component';

describe('PageLatestNewsComponent', () => {
  let component: PageLatestNewsComponent;
  let fixture: ComponentFixture<PageLatestNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageLatestNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageLatestNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
