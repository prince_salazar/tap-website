import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaLigaOfficialOutfitterComponent } from './la-liga-official-outfitter.component';

describe('LaLigaOfficialOutfitterComponent', () => {
  let component: LaLigaOfficialOutfitterComponent;
  let fixture: ComponentFixture<LaLigaOfficialOutfitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaLigaOfficialOutfitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaLigaOfficialOutfitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
