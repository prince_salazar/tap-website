import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaLigaOfficialOutfitterPageComponent } from './la-liga-official-outfitter-page.component';

describe('LaLigaOfficialOutfitterPageComponent', () => {
  let component: LaLigaOfficialOutfitterPageComponent;
  let fixture: ComponentFixture<LaLigaOfficialOutfitterPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaLigaOfficialOutfitterPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaLigaOfficialOutfitterPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
