import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Main Pages
import { PageClubComponent } from './page-club/page-club.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { PageWhereToPlayComponent } from './page-where-to-play/page-where-to-play.component';
import { PageLatestNewsComponent } from './page-latest-news/page-latest-news.component';

// Blog Pages
import { LaLigaOfficialOutfitterPageComponent } from './blog/la-liga-official-outfitter/la-liga-official-outfitter-page/la-liga-official-outfitter-page.component';


const routes: Routes = [
  { path: '', component: PageHomeComponent },
  { path: 'club', component: PageClubComponent },
  { path: 'play', component: PageWhereToPlayComponent },
  { path: 'news', component: PageLatestNewsComponent },
  { path: 'news/official-outfitter-la-liga', component: LaLigaOfficialOutfitterPageComponent }
];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
