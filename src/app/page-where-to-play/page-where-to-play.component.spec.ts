import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWhereToPlayComponent } from './page-where-to-play.component';

describe('PageWhereToPlayComponent', () => {
  let component: PageWhereToPlayComponent;
  let fixture: ComponentFixture<PageWhereToPlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageWhereToPlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWhereToPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
